package com.example.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity implements LocationListener{
    private LocationManager posizione; //-->A
    private TextView mostra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mostra = (TextView) findViewById(R.id.mostra);
        posizione = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //-->A chi nellâ€™applicazione richiede il servizio di localizzazione, il contesto, solitamente lâ€™Activity
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},120);
            //https://developers.google.com/android/guides/permissions
            //spiegazione dei permessi
        }else {
            posizione.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);}
        this.onLocationChanged((Location) null);
    }
    @Override
    public void onLocationChanged(Location location) {
        if(location==null) mostra.setText("Inaccessibile");
        else {
            StringBuilder info= new StringBuilder();
            if(location.hasSpeed()){
                info.append("VelocitÃ ").append(location.getSpeed()).append("m/s").append("\n");
            }
            if(location.hasAccuracy()) {
                info.append("Precisione").append(location.getAccuracy()).append("m").append("\n");
            }
            if(location.hasAltitude()) {
                info.append("Altitudine").append(location.getAltitude()).append("m").append("\n");
            }
            info.append("Latitudine").append(location.getLatitude()).append("\n");
            info.append("Longitudine").append(location.getLongitude()).append("\n");
            mostra.setText(info.toString());
        }
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }
    @Override
    public void onProviderEnabled(String s) {
    }
    @Override
    public void onProviderDisabled(String s) {
    }
}